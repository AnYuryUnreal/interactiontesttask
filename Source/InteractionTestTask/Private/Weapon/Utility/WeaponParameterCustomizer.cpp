// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/Utility/WeaponParameterCustomizer.h"

// Sets default values for this component's properties
UWeaponParameterCustomizer::UWeaponParameterCustomizer()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


FWeaponParameters UWeaponParameterCustomizer::CustomizeWeaponParameters(int32 tokens, FWeaponParameters BasicParameters, FWeaponParameters ModifPerToken)
{
	FWeaponParameters ModifiedParameters;
	
	int32 RandomParamNumber;
	ModifiedParameters.Auto = BasicParameters.Auto;
	ModifiedParameters.ClipSize = BasicParameters.ClipSize;
	ModifiedParameters.FireRate = BasicParameters.FireRate;
	ModifiedParameters.ShootRange = BasicParameters.ShootRange;
	ModifiedParameters.WeaponDamage = BasicParameters.WeaponDamage;

	if (ModifPerToken.Auto)
	{
		ModifiedParameters.Auto = rand() % 2;
		if (ModifiedParameters.Auto)
		{
			tokens--;
		}
	}
	for (tokens; tokens>0;tokens--)
	{
		RandomParamNumber = rand() % 4;
		if (RandomParamNumber == 0) { ModifiedParameters.ClipSize = ModifiedParameters.ClipSize + ModifPerToken.ClipSize; continue; }
		if (RandomParamNumber == 1) { ModifiedParameters.FireRate = ModifiedParameters.FireRate + ModifPerToken.FireRate; continue; }
		if (RandomParamNumber == 2) { ModifiedParameters.ShootRange = ModifiedParameters.ShootRange + ModifPerToken.ShootRange; continue; }
		if (RandomParamNumber == 3) { ModifiedParameters.WeaponDamage = ModifiedParameters.WeaponDamage + ModifPerToken.WeaponDamage; continue; }
	}
	return ModifiedParameters;
}

// Called when the game starts
void UWeaponParameterCustomizer::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UWeaponParameterCustomizer::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

