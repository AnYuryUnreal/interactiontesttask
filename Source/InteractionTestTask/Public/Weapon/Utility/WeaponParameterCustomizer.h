// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WeaponParameterCustomizer.generated.h"
USTRUCT(BlueprintType)
struct FWeaponParameters
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Auto;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 FireRate;
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ShootRange;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ClipSize;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float WeaponDamage;

	FWeaponParameters() { Auto = false; FireRate = 30; ShootRange = 1000; ClipSize = 3; }
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class INTERACTIONTESTTASK_API UWeaponParameterCustomizer : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponParameterCustomizer();
	UFUNCTION(BlueprintCallable)
	FWeaponParameters CustomizeWeaponParameters(int32 tokens, FWeaponParameters BasicParameters, FWeaponParameters ModifPerToken);
	




protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
